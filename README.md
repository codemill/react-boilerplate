# Boilerplate react application
This boilerplate react application was created using `create-react-app` and configured with the options as described in the original `README.md` file 

## Setup
To setup the project, check it out and init the dependencies using `yarn install`.

## Commands
```
# Start the development server using 
yarn start
```

```
# Bundle the app into static files for production
yarn build
```

```
# Start the testrunner
yarn test
```

```
# Test with code coverage
yarn test-coverage
```

```
# Removes this tool and copies build dependencies, configuration files and scripts into the app directory. If you do this, you can’t go back!
yarn eject
```

## Enabled features
- Linting output in the editor `.eslintrc`
- Automatic code formatting using prettier
- Editor configuration `.editorconfig`
- Code splitting vua dynamic import()
- SASS stylesheets
- Autoprefixer
- Testing with Jest and Enzyme
- Code coverage reporting
- Styleguidist
- React loadable
